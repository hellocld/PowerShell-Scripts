#-------------------------------------------------------------------------------
# Get-Help info
#-------------------------------------------------------------------------------
<#

.SYNOPSIS
Get-Fortune gives you a random fortune selected from a database of fortunes

.NOTES
To add new fortunes to the database, visit https://goo.gl/forms/za0rhhvyErgBPrCY2 and pass the "-update" parameter the next time you call Get-Fortune

#>



Param(
    [switch]$update,
    [switch]$moo,
    [switch]$all
)
#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
function Fortune ([int32]$fortuneId)
{
    return $fortunes[$fortuneId]."What's your fortune?"
}

function Display([string]$s)
{
    Write-Host "`n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" -ForegroundColor Yellow
    Write-Host $s -ForegroundColor Yellow
    Write-Host "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`n" -ForegroundColor Yellow
}

#-------------------------------------------------------------------------------
# Script main operations
#-------------------------------------------------------------------------------
$fortuneFile = $env:USERPROFILE + "\.fortunes\fortunes.txt"
$fortunes = Import-Csv $fortuneFile

# Get the latest database file from Google Drive
if($update)
{
    Invoke-WebRequest -Uri "https://docs.google.com/spreadsheets/d/e/2PACX-1vS2W5gKhyUtx86WijkmCWHlZWE1yftlRX2dQ5p4GXCOGGVIp-uqibtEZ29KYPtWfWY4qQNXT14k5M4q/pub?gid=558168563&single=true&output=csv" -OutFile $fortuneFile
    return
}

# Might display a cow someday
if($moo)
{
    Write-Host "No cows yet..."
    return
}

# Display every fortune in the database
if($all)
{
    for ($i = 0; $i -lt $fortunes.Count; $i++) {
        Display(Fortune($i))
    }
    Write-Host "Total fortunes: "$fortunes.Count
    return
}

# NO ARGS - Show a random fortune
$min = 0
$max = $fortunes.Length
Display(Fortune((Get-Random -Minimum $min -Maximum $max)))
return