$saveY = [System.Console]::CursorTop;
$saveX = [System.Console]::CursorLeft;
$continue = $true;
#Clear-Host;

While($continue)
{
    Get-Process | Sort-Object -Descending CPU | Select-Object -First 15;
    Start-Sleep -Milliseconds 10;
    [System.Console]::SetCursorPosition($saveX, $saveY + 3)

    if([System.Console]::KeyAvailable)
    {
        $key = [System.Console]::ReadKey()
        switch($key.key)
        {
            q{
                $continue = $false;
                [System.Console]::SetCursorPosition($saveX, $saveY + 20);
            }
        }
    }
}