#-------------------------------------------------------------------------------
#
# TaskbookSync.ps1 - commits, pulls, merges and pushes Tasbook changes to Git
#                    repo
#
#-------------------------------------------------------------------------------

Push-Location ~\.taskbook
git add . 
git commit -m $(Get-Date -Format G) 
git pull --rebase 
git push 
Pop-Location
