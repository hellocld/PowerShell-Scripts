Param(
    [uri]$track
)

function PlaySong([uri]$uri)
{
    Add-Type -AssemblyName presentationCore;
    $mplayer = New-Object System.Windows.Media.MediaPlayer;
    $mplayer.Open($uri)
    Start-Sleep 1;
    $duration = $mplayer.NaturalDuration.TimeSpan.TotalMilliseconds;
    $mplayer.Play();

    While($mplayer.Position.Milliseconds -lt $duration)
    {
        Write-Progress -Activity "Now playing..." `
            -Status "Progress: $($mplayer.Position.Seconds/$mplayer.Position.TotalSeconds)%" `
            -PercentComplete ($mplayer.Position.TotalSeconds/$mplayer.NaturalDuration.TimeSpan.TotalSeconds)
    }
    $mplayer.Stop();
}

PlaySong($track);